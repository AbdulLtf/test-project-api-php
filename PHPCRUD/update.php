<?php
include 'template/header.php';
require __DIR__ . '/users/users.php';

if (!isset($_GET['id'])) {
    include "template/not_found.php";
    exit;
}
$userId = $_GET['id'];

$user = getUserById($userId);
if (!$user) {
    include "template/not_found.php";
    exit;
}

$errors = [
    'firstname' => "",
    'lastname' => "",
];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $user = array_merge($user, $_POST);

    $isValid = validateUser($user, $errors);

    if ($isValid) {
        $user = updateUser($_POST, $userId);
        header("Location: index.php");
    }
}

?>

<?php include '_form.php' ?>
<?php include 'template/footer.php' ?>
