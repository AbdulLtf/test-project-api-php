<?php
include 'template/header.php';
require __DIR__ . '/users/users.php';


$user = [
    'id' => '',
    'firstname' => '',
    'lastname' => '',
    
];

$errors = [
    'firstname' => "",
    'lastname' => "",
    
];
$isValid = true;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $user = array_merge($user, $_POST);

    $isValid = validateUser($user, $errors);

    if ($isValid) {
        $user = createUser($_POST);

        header("Location: index.php");
    }
}

?>

<?php include '_form.php' ?>

