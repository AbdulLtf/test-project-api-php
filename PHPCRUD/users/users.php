<?php

function getUsers()
{   
    $data = [];
    $data = json_decode(file_get_contents(__DIR__ . '/users.json'), true);
    sort($data);
    return $data;
}

function getUserById($id)
{
    $users = getUsers();
    foreach ($users as $user) {
        if ($user['id'] == $id) {
            return $user;
        }
    }
    return null;
}

function createUser($data)
{
    $users = getUsers();
    
    $id = count($users)+1;

    $data['id'] = $id;
    $data['is_delete']="true";

    $users[] = $data;

    putJson($users);

    return $data;
}

function updateUser($data, $id)
{
    $updateUser = [];
    $users = getUsers();
    foreach ($users as $i => $user) {
        if ($user['id'] == $id) {
            $users[$i] = $updateUser = array_merge($user, $data);
        }
    }

    putJson($users);

    return $updateUser;
}

function deleteUser($id)
{
    $users = getUsers();

    foreach ($users as $i => $user) {
        if ($user['id'] == $id) {
            array_splice($users, $i, 1);
            $data['id']=$user['id'];
            $data['firstname']=$user['firtname'];
            $data['lastname']=$user['lastname'];
            $data['is_delete']="false";
        }
    }
    $users[] = $data;
    putJson($users);
}

function putJson($users)
{
    file_put_contents(__DIR__ . '/users.json', json_encode($users, JSON_PRETTY_PRINT));
}

function validateUser($user, &$errors)
{
    $isValid = true;
    // Start of validation
    if (!$user['firstname']) {
        $isValid = false;
        $errors['firstname'] = 'Firstname harus diisi';
    }
    if (!$user['lastname']) {
        $isValid = false;
        $errors['lastname'] = 'Lastname harus diisi';
    }
    
    // End Of validation

    return $isValid;
}
