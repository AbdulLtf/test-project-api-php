<?php
require 'users/users.php';

$users = getUsers();

include 'template/header.php';
?>


<div class="container" style="margin-top:20px">
    <p>
        <a class="btn btn-success" href="create.php">Create new User</a>
    </p>

    <table class="table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($users as $user): 
            if($user['is_delete']==="true"){
            ?>
            <tr>
                <td><?php echo $user['id'] ?></td>
                <td><?php echo $user['firstname'] ?></td>
                <td><?php echo $user['lastname'] ?></td>
                <td style="width:35%;">
                    <a href="view.php?id=<?php echo $user['id'] ?>" class="btn btn-sm btn-outline-info">View</a>
                    <a href="update.php?id=<?php echo $user['id'] ?>"class="btn btn-sm btn-outline-secondary">Update</a>
                    <form method="POST" action="delete.php" style="display:inline">
                        <input type="hidden" name="id" value="<?php echo $user['id'] ?>" >
                        <button class="btn btn-sm btn-outline-danger">Delete</button>
                    </form>
                </td>
            </tr>
        <?php }endforeach;; ?>
        </tbody>
    </table>
</div>

<?php include 'template/footer.php' ?>

