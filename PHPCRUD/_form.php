<div class="container" style="margin-top:20px">
    <div class="card">
        <div class="card-header">
            <h3>
                <?php if ($user['id']): ?>
                    Update user <b><?php echo $user['firstname'].$user['lastname'] ?></b>
                <?php else: ?>
                    Create new User
                <?php endif ?>
            </h3>
        </div>
        <div class="card-body">
            <form method="POST" 
                  action=""
                  >
                <div class="form-group">
                    <label>Firstname</label>
                    <input name="firstname" value="<?php echo $user['firstname'] ?>"
                           class="form-control <?php echo $errors['firstname'] ? 'is-invalid' : ''  ?>">
                    <div class="invalid-feedback">
                        <?php echo  $errors['firstname'] ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Lastname</label>
                    <input name="lastname" value="<?php echo $user['lastname'] ?>"
                           class="form-control <?php echo $errors['lastname'] ? 'is-invalid' : '' ?>">
                    <div class="invalid-feedback">
                        <?php echo  $errors['lastname'] ?>
                    </div>
                </div>
                <button class="btn btn-success">Submit</button>
            </form>
        </div>
        <a class="btn btn-primary" href="index.php" style="margin-top:10px">Kembali</a>
    </div>
</div>
