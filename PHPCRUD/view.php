<?php
include 'template/header.php';
require __DIR__ . '/users/users.php';

if (!isset($_GET['id'])) {
    include "template/not_found.php";
    exit;
}
$userId = $_GET['id'];

$user = getUserById($userId);
if (!$user) {
    include "template/not_found.php";
    exit;
}

?>
<div class="container" style="margin-top:20px">
    <div class="card">
        <div class="card-header">
            <h3>View User: <b><?php echo $user['firstname']." ".$user['lastname'] ?></b></h3>
        </div>
        <div class="card-body">
            <a class="btn btn-secondary" href="update.php?id=<?php echo $user['id'] ?>">Update</a>
            <form style="display: inline-block" method="POST" action="delete.php">
                <input type="hidden" name="id" value="<?php echo $user['id'] ?>">
                <button class="btn btn-danger">Delete</button>
            </form>
        </div>
        <table class="table">
            <tbody>
            <tr>
                <th>Firstname:</th>
                <td><?php echo $user['firstname'] ?></td>
            </tr>
            <tr>
                <th>Lastname:</th>
                <td><?php echo $user['lastname'] ?></td>
            </tr>
            
            </tbody>
        </table>
    <a class="btn btn-primary" href="index.php" style="margin-top:10px">Kembali</a>
    </div>
</div>


<?php include 'template/footer.php' ?>
